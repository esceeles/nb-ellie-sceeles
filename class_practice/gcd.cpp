#include <iostream>
using namespace std;

int gcd(int&, int&);

int main(){

int m = 0, n= 0;
int r = 0;

cout<<"Supply two integers to find the gcd:";
cin>>m>>n;
if (n > m)
   swap(n,m);
if (n == 0)
   cout<<"the answer is: "<<m;

//this is the iterative way to do it:
/*else{
   while ( n < 0 || n > 0){
      r = m % n;
      m = n;
      n = r;
   }
   cout<<"The answer is: "<<m<<endl;
}
*/
//this will be the recursive way:

gcd(m, n);
return 0;
}

int gcd(int &m, int &n){
   if (n != 0){
      int r = m % n;
      gcd(n, r);
   }
   if (n == 0)
      cout<<"GCD: "<<m<<endl;
}

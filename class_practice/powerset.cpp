#include <stdio.h>
#include <math.h>

int nunones(int n){
   int sum = 0;
   int x = 0x1;

   for (int i = 0; i < 8; i++){
      if (n & x) sum++; x<<1;}
return sum;
}

void printsubset(int subset, char *S){
   int x = 0x1;
   printf("\n{");
   for (int i = 0; i < 8; ++i){
      if (subset &x) printf(" %c", S[i]);
         x = x<<1;
      }
   printf(" }");
}

int main(){
   int ans;
   char S[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
   
   printf("List the Powerset of S = {a, b, c, d, e, f, g, h}\n");

   for (int i = 0; i < 256; i++)
      printsubset(i, S);

   printf("\n");
//   scanf("%d", &ans);

return 0;
}

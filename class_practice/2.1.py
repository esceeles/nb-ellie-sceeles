sec_per_hour = 60
hour_per_day = 24
sec_per_day = 60 * 24
print("Seconds in an hour: " + str(sec_per_hour))
print("Seconds in a day: " + str(hour_per_day * sec_per_hour))
print("d/h: " + str(sec_per_day/sec_per_hour))
print("d//h: " + str(sec_per_day//sec_per_hour))
words = "the.quick.brown.fox"
wordlist = words.split('.')
print(wordlist)
new_words = "+".join(wordlist)
print(new_words)

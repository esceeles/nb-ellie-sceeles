// graph.h

struct edge {
	int dir;
	int from;
	int to;
	struct edge *next;
};

struct graph {
	char name[10];
	int gtype;
	int num_verts;
	int num_edges;
	struct vertex **vertices;      //pointer to a pointer, array of structs (inside queue.h)
	struct edge *edgelist;         //edgelist is a variable that points to an edge, which in turn points to the rest of the edges
	int *adjmat;                   //adjacency matrix, pointer to 1d array
	};
	
void make_adjmat(struct graph *);
struct graph * readGraph(char *);
void printGraph(struct graph *);
void print_adjmat(int, int *);
void BFS(struct graph *);
void bfs(struct graph *, struct vertex *, struct node *, int *);
void DFS(struct graph *);
//void dfs(struct graph *, struct vertex *, struct node **, int *);

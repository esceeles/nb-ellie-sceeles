//stack.c
// Bryant W. York
// November 2015

#include <stdlib.h>
#include "graph.h"
#include "bool.h"
#include "stack.h"

struct node* make_stack() {
  struct node* p;
  p = NULL;
  return p;
}

/*
int isEmpty(struct node* head){
  if(head == NULL) {
    return 1;
  } else {
    return 0;
  }
}
*/

int push(struct node** head, struct node* element) {
  if(*head == NULL) {
    *head = element;
    (element)->next = NULL;
    return SUCCESS;
  } else {
    struct node* pushed = *head;
    *head = element;
    (element)->next = pushed;
    return SUCCESS;
  }
}

struct node* pop(struct node** head) {
  struct node* temp = *head;
  *head = (*head)->next;
  return temp;
}

/*
struct node* make_node() {
  struct node* p = malloc(sizeof(struct node));
  p->next = NULL;
  return p;
} 
*/

//stack.h
// Bryant W. York
// November 2015

struct node
{
  struct vertex *vert;
  struct node* next;
};

//Function declarations:
struct node* make_stack(void);
//int isEmpty(struct node*);
int push(struct node**, struct node*);
struct node* pop(struct node**);
//struct node* make_node(void);

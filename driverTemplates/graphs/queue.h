// Bryant W. York
// November 2015
//queue.h

struct vertex {
	int num;
	int mark;
	char name[10];
	};
	
struct node {
	struct vertex *vert;
	struct node *next;
};

//Function declarations:
struct node* make_queue(void);
int isEmpty(struct node*);
int insert(struct node**, struct node*);
struct node* removeq(struct node**);
struct node* make_node(void);
void print_queue(struct node *);

// graphDriver.c
// 
#include <stdio.h>
#include <string.h>
#include "queue.h"
#include "graph.h"

int main(){
	
	struct graph *g1;
	struct node *q, *n1;
	struct vertex * v;
	int i, nv, ans;
	
	g1 = readGraph("g1.txt");
	make_adjmat(g1);
         printGraph(g1);
	
	q = make_queue();
	nv = g1->num_verts;
	for(i=1; i<=nv;i++){
		v = g1->vertices[i];
		n1 = make_node();
		n1->vert = v;
		insert(&q,n1);
	}
	print_queue(q);
	printf("\nBefore BFS");
	scanf("\n%d",&ans);
	BFS(g1);
	printf("\nAfter BFS");
	scanf("\n%d",&ans);
	printGraph(g1);
	printf("\nDone\n");
		
	return 0;
}

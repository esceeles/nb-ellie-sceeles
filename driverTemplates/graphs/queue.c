//queue.c
// Bryant W. York
// November 2015

#include <stdio.h>
#include <stdlib.h>
#include "bool.h"
#include "queue.h"

//function definitions

struct node* make_queue() {
  struct node* p;
  p = NULL;
  return p;
}

int isEmpty(struct node* head){
  if(head == NULL) {
    return 1;
  } else {
    return 0;
  }
}

int insert(struct node** head, struct node* element) {
  if(*head == NULL) {
    *head = element;
    return SUCCESS;
  }
  struct node* end = *head;
  while(end->next != NULL) {
    end = end->next;
  }
  end->next = element;
  return SUCCESS;
}

struct node* removeq(struct node** head) {
  struct node* temp = *head;
  *head = (*head)->next;
  return temp;
}

struct node* make_node() {
  struct node* p = malloc(sizeof(struct node));
  p->next = NULL;
  return p;
} 

void print_queue(struct node * head){
	struct node * nd;
	struct vertex * v;
	
	while(head != NULL){
		nd = removeq(&head);
		v = nd->vert;
		printf("\nVertex num = %d  mark = %d",v->num, v->mark);
	}
}

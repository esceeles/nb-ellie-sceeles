//graph.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"
#include "graph.h"

void make_adjmat(struct graph *g){
	int nv, nv1, ne;
	int i, e1, e2;
	struct edge *e;
	int *am;
	
	nv = g->num_verts;
	nv1 = nv + 1;
	am = malloc((nv1*nv1)*sizeof(int));
	ne = g->num_edges;
	e = g->edgelist;
	for(i=0; i<ne; i++){
		e1 = e->from; 
		e2 = e->to;
		am[e1*nv1+e2] = 1;
		am[e2*nv1+e1] = 1;
		e = e->next;
	}
	g->adjmat = &am[nv1+1];        //originally = am... suggested maybe this should be = &am[nv1] or = &am[nv1+1];
}

struct graph * readGraph(char * fname){
	
	FILE *f1;
	int numg, nv, ne;
	int i, e1, e2;
	char nm[10];
	int type;
	struct graph * g;
	struct vertex **verts, *v;
	struct edge *head, *e, *p;
	
	
	g = malloc(sizeof(struct graph));
	
	f1 = fopen(fname, "r");
	fscanf(f1,"%d", &numg);  // read the number of graphs
	fscanf(f1,"%s",nm);
	strcpy(g->name,nm);
	fscanf(f1, "%d" , &type);  //1=undirected, 2=directed
	g->gtype = type;
	fscanf(f1,"%d",&nv);
	g->num_verts = nv;
	verts = malloc((nv+1)*sizeof(struct vertex *));
	for (i = 1; i < nv+1; i++) {
		v = malloc(sizeof(struct vertex));
		fscanf(f1,"%s",nm);
		strcpy(v->name,nm);
		v->num = i;
		verts[i] = v;
	}
	g->vertices = verts;
	
	fscanf(f1,"%d",&ne);
	g->num_edges = ne;
	fscanf(f1,"%d %d",&e1,&e2);
	head = malloc(sizeof(struct edge));
	head->from = e1;
	head->to = e2;
	p = head;
	
	for(i=1; i<ne; i++){
		fscanf(f1,"%d %d",&e1,&e2);
		e = malloc(sizeof(struct edge));
		e->from = e1;
		e->to = e2;
		p->next = e;
		p = e;	
	}

	g->edgelist = head;
	
	fclose(f1);
	return g;
	
}

void printGraph(struct graph * g){
	int nv, ne;
	int i, e1, e2;
	struct vertex *v;
	struct edge *e;
	
	printf("\nName = %s",g->name);
	printf("\nGraph Type = %d",g->gtype);
	printf("\nNumber of Vertices = %d",g->num_verts);
	nv = g->num_verts;
	for (i = 1; i < nv+1; i++) {
		v = g->vertices[i];
		printf("\nVertex number = %d",v->num);
		printf("\nVertex name = %s",v->name);
		printf("\n Vertex mark = %d",v->mark);
	}
	
	ne = g->num_edges;
	printf("\nNumber of edges = %d", g->num_edges);
	e = g->edgelist;
	for(i=0; i<ne; i++){
		e1 = e->from; 
		e2 = e->to;
		printf("\nEdge from = %d  to = %d", e1, e2);
		e = e->next;
	}

//	printf("\nAdjacency Matrix\n");
//	print_adjmat(nv+1,g->adjmat);
	return;
	
}

void print_adjmat(int n, int *mat)
{
	int i, j;
	for (i=1; i<n; i++){
		for(j=1;j<n;j++) {
			printf("%2d",mat[i*n+j]);
		}
		printf("\n");
	}
	
}


int adjacent(int v1, int v2, int n, int *mat){
	if(mat[v1*n+v2] == 1) return 1;
	else return 0;
}
	
//bfs

void bfs(struct graph * g, struct vertex *v, struct node *q,int *count){
	struct node *n1, *nd, *ndd;
	struct vertex *w, *f;
	int i, wn, fn;
	int nv, empty, ans;
	
	printf("\nInside bfs");
	(*count)++;
	v->mark = *count;
	n1 = make_node();
	n1->vert = v;
	insert(&q,n1);
	nv = g->num_verts;
	empty = isEmpty(q);
	printf("\nInside bfs   nv = %d  empty = %d",nv,empty);
	
	while (empty == 0){
		printf("\nInside while empty = %d",empty);
		//scanf("%d",&ans);
		nd = q;
		printf("\nAfter remove   nd = ");
		//scanf("%d",&ans);
		f = nd->vert;
		//if (f == NULL) return;
		//if (f->mark != 0) continue;
		fn = f->num;
		for(i=1; i<=nv; i++){
			if(i == fn) continue;
			w = g->vertices[i];
			wn = w->num;
			if (!adjacent(fn,wn,nv+1,g->adjmat)) continue;
			if (w->mark == 0){
				count++;
				w->mark = *count;
				nd->vert = w;
				nd->next = NULL;
				insert(&q,nd);
				printf("\nInside set mark");
			}
		}
		ndd = removeq(&q);
		print_queue(q);
		scanf("\n%d",&ans);
		empty = isEmpty(q);
		printf("\nEnd of while   empty = %d",empty);
	}
}


//BFS

void BFS(struct graph *g){
	int nv, count;
	int i;
	struct vertex *v;
	struct node *qhead;
	
	qhead = make_queue();
	
	nv = g->num_verts;
	//mark all vertices 0
	for (i = 1; i <=nv; i++) {
		v = g->vertices[i];
		v->mark = 0;
	}
	
	count = 0;
	
	for (i=1; i <= nv; i++){
		v = g->vertices[i];
		if (v->mark == 0) bfs(g, v, qhead, &count);
	}
}

/*
//dfs
void dfs(struct graph *v, struct node *s){
	
	
	
}

//DFS

void DFS(struct graph *g){
	int nv, ne, count;
	int i;
	struct vertex *v;
	struct edge *e;
	struct node *shead;
	
	shead = make_stack();
	
	nv = g->num_verts;
	//mark all vertices 0
	for (i = 1; i < nv+1; i++) {
		v = g->vertices[i];
		v->mark = 0;
	}
	count = 0;
	for (i=1; i < nv+1; i++){
		v = g->vertices[i];
		if (v->mark == 0) dfs(v,shead);
	
	}
}
*/

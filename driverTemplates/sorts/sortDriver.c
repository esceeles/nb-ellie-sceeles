// sortDriver.c
// Bryant W. York
// November 2015

#include <stdio.h>
#include "sorts.h"

int num, D[10000]; //data array

int main() {
	int i, stype, sorted;
	FILE *f1;
	f1 = fopen("york_Data0.txt", "r");

	fscanf(f1,"%d",&num);  // read the number of integers
	// read the integers into array D (for Data)
	for (i=0; i<num; i++) fscanf(f1,"%d",&D[i]);
	fclose(f1);
	
	sorted = 0;  //set flag as unsorted
	
	//read from terminal the sort type selector
	printf("\nEnter the type of sort");
	printf("\n1 = selection sort");
	printf("\n2 = bubble sort");
	printf("\n3 = merge sort");
	printf("\n4 = heap sort");
	printf("\n5 = insertion sort");
	printf("\n6 = quicksort");
	printf("\n");
	scanf("%d",&stype);
	
	switch (stype){
	case 1:
		selectionSort(num,D);
		sorted = 1;
		break;
	case 2:
		bubbleSort(num,D);
		sorted = 1;
		break;
	case 3:
		mergeSort(num,D);
		sorted = 1;
		break;
	case 4:
		heapSort(num,D);
		sorted = 1;
		break;
	case 5:
		insertionSort(num,D);
		sorted = 1;
		break;
	case 6:
		quickSort(0, num-1 ,D);
		sorted = 1;
		break;
	default:
		sorted = 0;
	}

	if (sorted == 0) 
		{printf("\nIllegal sort type\n"); return 0;}
	
	printf("\nSorted Array\n");
	for (i=0; i<num; i++) printf("%5d ", D[i]);

        printf("\n\n");
	return 0;
}

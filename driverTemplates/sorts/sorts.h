// sorts.h
// Bryant W. York
// November 2015

//function declarations

void selectionSort(int, int *);
void bubbleSort(int, int *);
void mergeSort(int, int *);
void heapSort(int, int *);
void insertionSort(int, int *);
void quickSort(int, int, int *);

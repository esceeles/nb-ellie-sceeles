// quickSort.c
// Bryant W. York
// November 2015


int partition(int* A, int low, int high){
   int pivot = A[high];
   int i = (low -1);

   for (int j = low; j <= (high-1); j++){
      if (A[j] <= pivot){
         i++;
         int temp = A[i];
         A[i] = A[j];
         A[j] = temp;
      }
   }
   int temp = A[i+1];
   A[i+1] = A[high];
   A[high] = temp;
   return (i+1);
};

void quickSort(int low, int high, int* A)
{

if (low < high){
   int p = partition(A, low, high);
   quickSort(low, p-1, A);
   quickSort(p+1, high, A);
}
return;
}

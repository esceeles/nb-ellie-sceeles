// bubbleSort.c
// for integers
// Bryant W. York
// November 2015

void swapint(int *, int *);

void bubbleSort(int n, int* A)
{
	int i, j;
	int nm2 = n - 2;
	for(i=0; i<=nm2; i++){
		for(j=0; j<=nm2-i; j++){
		if(A[j+1]<A[j]) swapint(&A[j],&A[j+1]);
		}
			
	}
}


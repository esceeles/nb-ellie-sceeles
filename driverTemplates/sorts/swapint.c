// swapint.c

void swapint(int * x, int * y)
{
	int temp;
	temp = *x;
	*x = *y;
	*y = temp;	
}

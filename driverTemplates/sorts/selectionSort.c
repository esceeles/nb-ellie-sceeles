// selectionSort.c
// for integers
// Bryant W. York
// November 2015

void swapint(int *, int *);

void selectionSort(int n, int* A)
{
	int i, j, minx;
	int nm1 = n - 1;
	int nm2 = n - 2;
	for(i=0; i<=nm2; i++){
		minx = i;
		for(j=i+1; j<=nm1; j++){
		if(A[j]<A[minx]) minx = j;
		}
		swapint(&A[i],&A[minx]);	
	}
}


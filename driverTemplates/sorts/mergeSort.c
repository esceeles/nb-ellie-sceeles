// mergeSort.c
// Bryant W. York
// November 2015

#include <stdio.h>
#include <stdlib.h>

#define floor(a,b) ((a) / ((b)))
#define ceil(a,b) ((a) % (b) == 0 ? (a) / (b) : (a) / (b) + 1)

void copyInt(int *, int, int, int *, int);
void merge(int, int *, int, int *, int, int *);

void copyInt(int * X, int startx, int endx, int * Y, int starty){
	//copies the array of integers X starting at startx
	// and ending at endx to the array Y starting at starty
	int i = 0;
	while ((startx + i) <= endx) {
		(*(Y+starty+i)) = (*(X+startx+i));
		i++;
	}
}

void merge(int Blen, int *B, int Clen, int *C, int Alen, int *A){
	int i, j, k;
	i = 0;
	j = 0;
	k = 0;
	while ((i<Blen) && (j<Clen)){
		if (B[i] <= C[j]) {
			A[k] = B[i];
			i++;
		}
		else {
			A[k] = C[j];
			j++;
		}
		k++;
	}//end while
	if (i == Blen) copyInt(C,j,Clen-1,A,k);
	else copyInt(B,i,Blen-1,A,k);
}


void mergeSort(int n, int* A)
{
	int *B;
	int *C;
	int nm1, fn2, cn2, fn2m1, cn2m1;
	int i, ans;
	
	if (n>1){
		nm1 = n - 1;
		fn2 = floor(n,2);
		cn2 = ceil(n,2);
		fn2m1 = fn2 - 1;
		cn2m1 = cn2 - 1;
		printf("\n n = %d  fn2 = %d  cn2 = %d ", n,fn2,cn2);
		B = (int *) malloc(fn2*sizeof(int));
		C = (int *) malloc(cn2*sizeof(int));
		copyInt(A,0,fn2m1,B,0);
		printf("\nB\n");
		for (i=0; i<fn2; i++) printf("%d ",B[i]);
		printf("\n");
		copyInt(A,fn2,nm1,C,0);
		printf("\nC\n");
		for (i=0; i<cn2; i++) printf("%d ",C[i]);
		printf("\n");
		scanf("%d",&ans);
		mergeSort(fn2,B);
		mergeSort(cn2,C);
		merge(fn2,B,cn2,C,n,A);
		free(B);
		free(C);
	} else return;
}


#ifndef _maxInteger_c_
#define _maxInteger_c_
// maxInteger.c
// Bryant W. York
// November 2015

int maxInteger(int n, int *A){
	int m, i;
	m = A[0];
	for (i=0; i<n; i++)
		if (A[i] > m) m = A[i];
	return m;
}
#endif

// nonrecBS.c
//for integers
// Bryant W. York
// November 2015

int nonrecBS(int K, int n, int *A){
	int l, r, m;
	
	l = 0;
	r = n-1;
	
	while (l <= r){
		m = (l+r)/2;  // int div gives floor
		if (K == A[m]) return m;
		else if (K < A[m]) r = m-1;
		else l = m+1;
 	}
	
	return -1;
}

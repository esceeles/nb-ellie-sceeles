from turtle import *
color("purple", "black")
speed(10)
x = 100
y = 30

while True:
   begin_fill()
   forward(100)
   left(170)
   forward(200)
   left(170)
   end_fill()
   forward(x)
   left(y)
   x += .01
   y += .25
   if x == 200:
      x = 100
   if y == 45:
      y = 30

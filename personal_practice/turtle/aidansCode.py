from turtle import *
from random import choice
penup()
l = ['purple', 'red', 'yellow', 'blue', 'green', 'orange']
goto(-100 ,100)
pendown()
bgcolor('black')
color('white')
speed(1000)
x = 0
y = 0
for i in range(1000):
    color(choice(l))
    forward(10 + x)
    left(120)
    color('black')
    forward(10)
    left(91)

    forward(5)
    left(93)
    forward(10)
    left(91)
    forward(50)
    left(93)
    color(choice(l))
    forward(10 + x / 2)
    left(10)
    color('black')
    forward(200)
    left(150)
    forward(300)
    left(100)
    
    forward(200)
    left(150)
    color(choice(l))
    forward(20)
    left(170)
    forward(150)
    left(90)
    forward(10)
    left(120)

    y += .25
    x += .25

    if y == 40.9:
        y = 0

hideturtle()
done()

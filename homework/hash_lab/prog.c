file = open("uuid.txt")

hash = {line:sum([ord(l)**(i+1) for i,l in enumerate(line)]) for line in file}

uniqueHashes = len(set(hash.values()))
print("unique hashes: {} / total hashed: {}" .format(uniqueHashes, len(hash)))
print("collision rate: %{}".format(1-(uniqueHashes/len(hash))))

#ifndef _recBS_c_
#define _recBS_c_

#include <stdio.h>
#include <stdlib.h>

//program for a recursive binary search

int recBS(int l, int u, int key, int n, int *A){
   int mid;  
   if (l <= u){
      mid = (l+u)/2;
      if (key == A[mid]){
         return mid;
      }
      else if (key < A[mid]){
         return recBS(l, mid-1, key, n,A);
      }
      else
         return recBS(mid+1, u, key, n, A);
   }
   return -1;
}
#endif
/*
int main(){

int A[20];

//filling array with stuff
for (int i = 0; i < 20; i++){
   A[i] = 1 + i;
}

//printing array
for (int i = 0; i < 20; i++){
   printf("%d ", A[i]);
}
printf("\n");


return 0;
}
*/

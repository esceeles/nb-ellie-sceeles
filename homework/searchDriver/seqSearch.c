#ifndef _seqSearch_c_
#define _seqSearch_c_

// seqSearch.c
//for integers
// Bryant W. York
// November 2015

int seqSearch(int start, int elem, int n, int *A){
	// start is the position within the array to begin
	// returns -1 if elem is not found
	int i;
	if (start >= n) return -1;
	
	for (i=start; i<n; i++)
		if (A[i] == elem) return i;
	
	return -1;
}

#endif

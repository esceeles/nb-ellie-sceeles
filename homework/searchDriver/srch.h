// srch.h
// Bryant W. York
// November 2015

int maxInteger(int, int *);
int seqSearch(int, int, int, int *);
int nonrecBS(int,int, int *);
int recBS(int, int, int, int, int *);

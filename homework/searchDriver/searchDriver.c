// searchDriver.c
// for integers
// Bryant W. York
// November 2015

#include <stdio.h>
#include "srch.h"

int main(){
	int stype, i, num, D[10000];
	FILE *f1;
	int mx, element;
	
	//read from terminal the search type selector
	printf("\nEnter the type of search");
	printf("\n1 = find the maximum integer");
	printf("\n2 = perform sequential search for value");
	printf("\n3 = perform nonrecursive binary search");
	printf("\n4 = perform recursive binary search");
	printf("\n");
	scanf("%d",&stype);
	switch (stype){
		case 1:
			f1 = fopen("Data1.txt", "r");
			fscanf(f1,"%d", &num);  // read the number of integers
			for (i=0; i<num; i++) {fscanf(f1,"%d",&D[i]);}	
			fclose(f1);
			mx = maxInteger(num,D);
			printf("\nThe max integer is %d",mx);
			break;
		case 2:
			f1 = fopen("Data1.txt", "r");
			fscanf(f1,"%d", &num);  // read the number of integers
			for (i=0; i<num; i++) {fscanf(f1,"%d",&D[i]);}	
			fclose(f1);
			printf("\nEnter the element to be searched for\n");
			scanf("%d",&element);
			mx = seqSearch(0,element,num,D);
			printf("\nThe index of %d is %d",element, mx);
			break;
		case 3:
			f1 = fopen("Data8.txt", "r");
			fscanf(f1,"%d", &num);  // read the number of integers
			for (i=0; i<num; i++) {fscanf(f1,"%d",&D[i]);}	
			fclose(f1);
			printf("\nEnter the element to be searched for\n");
			scanf("%d",&element);
			mx = nonrecBS(element,num,D);
			if (mx >= 0) printf("\nThe index of %d is %d",element, mx);
			else printf("\nKey = %d is not in list\n",element);
			break;
		case 4:
			f1 = fopen("Data8.txt", "r");
			fscanf(f1,"%d", &num);  // read the number of integers
			for (i=0; i<num; i++) {fscanf(f1,"%d",&D[i]);}	
			fclose(f1);
			printf("\nEnter the element to be searched for\n");
			scanf("%d",&element);
			mx = recBS(0,num-1,element,num,D);
			if (mx >= 0) printf("\nThe index of %d is %d",element, mx);
			else printf("\nKey = %d is not in list\n",element);
			break;
		default:
			printf("\nIllegal selection\n");
			break;
	}
	
	printf("\nDone\n");
		
	return 0;
}

